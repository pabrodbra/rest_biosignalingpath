package prb.rest;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "biosignalingpath")

public class BiosignalingPath {
	private String id;
	private String name;
	private String organism;
	private String description;
	private ArrayList<String> diseases;


	@XmlElement
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@XmlElement
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@XmlElement
	public String getOrganism() {
		return organism;
	}
	public void setOrganism(String organism) {
		this.organism = organism;
	}

	@XmlElement
	public ArrayList<String> getDiseases() {
		return diseases;
	}
	public void setDiseases(ArrayList<String> diseases) {
		this.diseases = diseases;
	}
	
	@XmlElement
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
