package prb.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/bsp")
	
public class WebController {
public static Map<String, BiosignalingPath> biosignalingpaths = new HashMap<String, BiosignalingPath>();
	
	static{
		BiosignalingPath bsp1 = new BiosignalingPath();
		bsp1.setId("1");
		bsp1.setName("Synaptic Signaling");
		bsp1.setDescription("One unique example of paracrine signaling is synaptic signaling, in which nerve cells transmit signals. This process is named for the synapse, the junction between two nerve cells where signal transmission occurs.\n"
				+ "When the sending neuron fires, an electrical impulse moves rapidly through the cell, traveling down a long, fiber-like extension called an axon. When the impulse reaches the synapse, it triggers the release of ligands called neurotransmitters, which quickly cross the small gap between the nerve cells. When the neurotransmitters arrive at the receiving cell, they bind to receptors and cause a chemical change inside of the cell (often, opening ion channels and changing the electrical potential across the membrane).");
		bsp1.setOrganism("Homo Sapiens");
		ArrayList<String> diseases1 = new ArrayList<String>();
		diseases1.add("Psychiatric Disorders");
		diseases1.add("Neurologic Diseases");
		diseases1.add("Neurodegenerative");
		bsp1.setDiseases(diseases1);
		biosignalingpaths.put(bsp1.getId(), bsp1);
		
		BiosignalingPath bsp2 = new BiosignalingPath();
		bsp2.setId("2");
		bsp2.setName("Growth Hormone Signaling");
		bsp2.setDescription("Growth hormone (GH), also known as somatotropin (or as human growth hormone [hGH or HGH] in its human form), is a peptide hormone that stimulates growth, cell reproduction, and cell regeneration in humans and other animals. It is thus important in human development. It is a type of mitogen which is specific only to certain kinds of cells. Growth hormone is a 191-amino acid, single-chain polypeptide that is synthesized, stored and secreted by somatotropic cells within the lateral wings of the anterior pituitary gland.");
		bsp2.setOrganism("Homo Sapiens");
		ArrayList<String> diseases2 = new ArrayList<String>();
		diseases2.add("Excess: Pituitary tumors");
		diseases2.add("Prolonged excess: Thickening of the jaw, finger and toes");
		diseases2.add("Deficiency: Alterations in somatomedin");
		bsp2.setDiseases(diseases2);
		biosignalingpaths.put(bsp2.getId(), bsp2);
		

		BiosignalingPath bsp3 = new BiosignalingPath();
		bsp3.setId("3");
		bsp3.setName("BIOSIGNALING EXAMPLE 3");
		bsp3.setDescription("EXAMPLE DESCRIPTION");
		bsp3.setOrganism("EXAMPLE ORGANISM");
		ArrayList<String> diseases3 = new ArrayList<String>();
		diseases3.add("DISEASE 1");
		diseases3.add("DISEASE 2");
		diseases3.add("DISEASE 3");
		diseases3.add("DISEASE 4");
		diseases3.add("DISEASE 5");
		diseases3.add("DISEASE 6");
		bsp3.setDiseases(diseases3);
		biosignalingpaths.put(bsp3.getId(), bsp3);
	}
	

	@GET
	@Path("/biosignalingpaths")
	@Produces("text/xml")
	public ArrayList<BiosignalingPath> showBiosignalingPaths(){
		return new ArrayList<BiosignalingPath>(biosignalingpaths.values());
	}

	@GET
	@Path("/biosignalingpaths/{id}")
	@Produces("text/xml")
	public BiosignalingPath showSelectedBiosignalingPaths(@PathParam("id") String id){
		return biosignalingpaths.get(id);
	}
	
	@GET
	@Path("/json/biosignalingpaths")
	@Produces("application/json")
	public ArrayList<BiosignalingPath> showBiosignalingPathsJSON(){
		return new ArrayList<BiosignalingPath>(biosignalingpaths.values());
	}
	
	@GET
	@Path("/json/biosignalingpaths/{id}")
	@Produces("application/json")
	public BiosignalingPath showSelectedBiosignalingPathsJSON(@PathParam("id") String id){
		return biosignalingpaths.get(id);
	}

}
